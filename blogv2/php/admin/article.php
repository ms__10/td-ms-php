<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../../utils/db_connect.php");
// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();
// J'appelle ma fonction pour savoir si mon utilisateur est admin
isAdmin();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

switch ($method["choice"]) {
    case 'select':
        // Je récupère tous les articles
        $req = $db->query("SELECT a.*, CONCAT(firstname, ' ', lastname) AS author FROM articles a INNER JOIN users u ON a.user_id = u.id ORDER BY created_at DESC");

        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        foreach ($articles as &$article) {
            $req = $db->prepare("SELECT name FROM categories c INNER JOIN article_category ac ON ac.category_id = c.id WHERE ac.article_id = ?");
            $req->execute([$article["id"]]);

            $article["categories"] = $req->fetchAll(PDO::FETCH_COLUMN);
        }

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case "select_id":
        //? Si je n'ai pas le paramètre "article_id" OU s'il est vide dans ma requête GET stockée dans ma variable $method alors
        if (!isset($method["id"]) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // Je récupère l'article ciblé par l'id en paramètre
        $req = $db->prepare("SELECT * FROM articles WHERE id = ?");
        $req->execute([$method["id"]]);

        // J'affecte à ma variable $article le résultat unique (ou pas de résultat) de ma requete SQL
        $article = $req->fetch(PDO::FETCH_ASSOC);

        // Je récupère les ids des catégories liées à l'article
        $req = $db->prepare("SELECT category_id FROM article_category WHERE article_id = ?");
        $req->execute([$method["id"]]);
        // J'ajoute le tableau de résultat dans ma variable article
        $article["categories"] = $req->fetchAll(PDO::FETCH_COLUMN);

        // J'envoie une réponse avec un success true ainsi que les données de l'article
        echo json_encode(["success" => true, "article" => $article]);

        break;

    case 'insert':
        //? Si la requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas les paramètres "name" et "desc" OU qu'ils sont vides alors
        if (!isset($method["name"], $method["desc"]) || empty(trim($method["name"])) || empty(trim($method["desc"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        $img = false; // Je défini img à false par défaut
        if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload

        // J'écris une requete préparée d'insertion de mes données dans la table article
        $req = $db->prepare("INSERT INTO articles(name, description, image, user_id) VALUES (:name, :desc, :img, :user_id)");
        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":name", $method["name"]);
        $req->bindValue(":desc", $method["desc"]);
        $req->bindValue(":user_id", $_SESSION["user_id"]);
        if ($img) $req->bindValue(":img", $img); // Je bind le chemin si la réponse de upload() n'est pas false
        else $req->bindValue(":img", null); // Sinon je mets l'image à null
        $req->execute();

        // Je stock l'id de l'article que je viens de créer
        $article_id = $db->lastInsertId();

        // J'itère sur les ids de catégories que je reçois
        foreach (json_decode($method["cat_ids"]) as $cat) {
            // Pour chacun des ids je fais une requête d'insertion dans la table relationnelle entre catégorie et article
            $req = $db->prepare("INSERT INTO article_category(category_id, article_id) VALUES (:cat, :article)");
            $req->bindValue(":cat", $cat);
            $req->bindValue(":article", $article_id);
            $req->execute();
        }

        // J'envoie une réponse avec un success true
        echo json_encode(["success" => true]);

        break;

    case 'update':
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas les paramètres "name", "desc" et "id" OU qu'ils sont vides alors
        if (!isset($method["name"], $method["desc"], $method["id"]) || empty(trim($method["name"])) || empty(trim($method["desc"])) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        $img = false; // Je défini img à false par défaut
        if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload

        $img_req = ''; // Par défaut rien n'est ajouté dans la requête SQL
        if ($img) $img_req = ", image = :img"; // S'il y a une image d'upload lors de la mise à jour je rajoute le bout de requête SQL correspondant

        // J'écris une requete préparée de mise à jour de l'article
        $req = $db->prepare("UPDATE articles SET name = :name, description = :desc $img_req WHERE id = :id");
        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":name", $method["name"]);
        $req->bindValue(":desc", $method["desc"]);
        $req->bindValue(":id", $method["id"]);
        if ($img) $req->bindValue(":img", $img);
        $req->execute();

        //TODO Mettre à jour les catégories de l'article, supprimer celles décochées et insérer les nouvelles cochées.
        $article_categories = json_decode($method["cat_ids"]);

        $req = $db->prepare("SELECT category_id FROM article_category WHERE article_id = ?");
        $req->execute([$method["id"]]);
        $categories = $req->fetchAll(PDO::FETCH_COLUMN);

        foreach ($categories as $category) {
            if (!in_array($category, $article_categories)) {
                $req = $db->prepare("DELETE FROM article_category WHERE category_id = ? AND article_id = ?");
                $req->execute([$category, $method["id"]]);
            }
        }

        foreach ($article_categories as $category) {
            if (!in_array($category, $categories)) {
                $req = $db->prepare("INSERT INTO article_category(category_id, article_id) VALUES (?, ?)");
                $req->execute([$category, $method["id"]]);
            }
        }

        echo json_encode(["success" => true]);

        break;

    case 'delete':
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas de paramètre "id" OU qu'il est vide alors
        if (!isset($method["id"]) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée de suppression de l'article
        $req = $db->prepare("DELETE FROM articles WHERE id = ?");
        $req->execute([$method["id"]]);

        //? Si j'ai 1 résultat avec c'est un succès
        if ($req->rowCount()) echo json_encode(["success" => true]);
        //? Sinon
        else echo json_encode(["success" => false, "error" => "Erreur lors de la suppression"]);

        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
