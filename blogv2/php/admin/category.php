<?php
error_reporting(-1);

require_once("../../utils/db_connect.php");

require("../../utils/function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;


switch ($method["choice"]) {

    case "select":
        
        $request = $db->query("SELECT * FROM categories");

        $category = $request->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "category" => $category]);
        break;


    case "select_id":

        if (isset($method["category_id"]) && !empty(trim($method["category_id"]))) {

            $request = $db->prepare("SELECT * FROM categories WHERE id = ?");
            $request->execute([$method["category_id"]]);

            $category = $request->fetch(PDO::FETCH_ASSOC);
            echo json_encode(["success" => true, "category" => $category]);
        } else {

            echo json_encode(["success" => false, "error" => "Id manquant"]);
        }

        break;

    case "insert":

        if ($_SERVER["REQUEST_METHOD"] != "POST") {

            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die;
        }

        if (!isset($method["name"]) || empty(trim($method["name"]))) {

            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die;
        }

        $request = $db->prepare("INSERT INTO categories(name) VALUES (:name)");
        $request->bindValue(":name", $method["name"]);
        $request->execute();

        echo json_encode(["success" => true]);
        
        break;

        case 'update':
            //? Si ma requête HTTP n'est pas en POST alors
            if ($_SERVER["REQUEST_METHOD"] != "POST") {
                // J'envoie une réponse avec un success false et un message d'erreur
                echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
                die; //! J'arrête l'exécution du script
            }
    
            //? Si je n'ai pas les paramètres "name" et "id" OU qu'ils sont vides alors
            if (!isset($method["name"], $method["id"]) || empty(trim($method["name"])) || empty(trim($method["id"]))) {
                // J'envoie une réponse avec un success false et un message d'erreur
                echo json_encode(["success" => false, "error" => "Données manquantes"]);
                die; //! J'arrête l'exécution du script
            }
    
            // J'écris une requete préparée de mise à jour de la categorie
            $req = $db->prepare("UPDATE categories SET name = :name WHERE id = :id");
            // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
            $req->bindValue(":name", $method["name"]);
            $req->bindValue(":id", $method["id"]);
            $req->execute();
    
            //? Si j'ai 1 résultat avec c'est un succès
            if ($req->rowCount()) echo json_encode(["success" => true]);
            //? Sinon
            else echo json_encode(["success" => false, "error" => "Erreur lors de la mise à jour"]);
    
            break;
    
        case 'delete':
            //? Si ma requête HTTP n'est pas en POST alors
            if ($_SERVER["REQUEST_METHOD"] != "POST") {
                // J'envoie une réponse avec un success false et un message d'erreur
                echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
                die; //! J'arrête l'exécution du script
            }
    
            //? Si je n'ai pas de paramètre "id" OU qu'il est vide alors
            if (!isset($method["id"]) || empty(trim($method["id"]))) {
                // J'envoie une réponse avec un success false et un message d'erreur
                echo json_encode(["success" => false, "error" => "Id manquant"]);
                die; //! J'arrête l'exécution du script
            }
    
            // J'écris une requete préparée de suppression de la catégorie
            $req = $db->prepare("DELETE FROM categories WHERE id = ?");
            $req->execute([$method["id"]]);
    
            //? Si j'ai 1 résultat avec c'est un succès
            if ($req->rowCount()) echo json_encode(["success" => true]);
            //? Sinon
            else echo json_encode(["success" => false, "error" => "Erreur lors de la suppression"]);
    
            break;
    
        default:
            //! Aucune case ne correspond à mon choix
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
            break;
    }
    
