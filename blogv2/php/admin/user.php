<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../../utils/db_connect.php");
// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();
// J'appelle ma fonction pour savoir si mon utilisateur est admin
isAdmin();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

//? En fonction du paramètre "choice" de ma requête j'execute les instructions de la case correspondante
switch ($method["choice"]) {
    case 'select':
        // Je récupère tous les utilisateurs
        $req = $db->query("SELECT id, firstname, lastname, birthdate, email FROM users");

        // J'affecte la totalité de mes résultats à la variable $users
        $users = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les utilisateurs
        echo json_encode(["success" => true, "users" => $users]);
        break;

    case 'select_id':
        //? Si je n'ai pas de paramètre "user_id" OU qu'il est vide alors
        if (!isset($method["user_id"]) || empty(trim($method["user_id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // Je récupère l'utilisateur ciblé par l'id en paramètre
        $req = $db->prepare("SELECT id, firstname, lastname, birthdate, email FROM users WHERE id = ?");
        $req->execute([$method["user_id"]]);

        // J'affecte à ma variable $user le résultat unique (ou pas de résultat) de ma requete SQL
        $user = $req->fetch(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que l'utilisateur
        echo json_encode(["success" => true, "user" => $user]);
        break;

    case 'update':
        //? Si je n'ai les paramètres "firstname", "lastname", "birthdate", "email" et "user_id" OU qu'ils sont vides alors
        if (!isset($method["firstname"], $method["lastname"], $method["email"], $method["birthdate"], $method["user_id"]) || empty(trim($method["firstname"])) || empty(trim($method["lastname"])) || empty(trim($method["email"])) || empty(trim($method["birthdate"])) || empty(trim($method["user_id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        $regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
        //? Si mon email ne correspond pas à l'ER alors
        if (!preg_match($regex, $method["email"])) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Email au mauvais format"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée de mise à jour de l'utilisateur
        $req = $db->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email, birthdate = :birthdate WHERE id = :user_id");
        $req->bindValue(":firstname", $method["firstname"]);
        $req->bindValue(":lastname", $method["lastname"]);
        $req->bindValue(":email", $method["email"]);
        $req->bindValue(":birthdate", $method["birthdate"]);
        $req->bindValue(":user_id", $method["user_id"]);
        $req->execute();

        // J'envoie une réponse avec un success true
        echo json_encode(["success" => true]);
        break;

    case "search":
        //? Si je n'ai pas le paramètre search OU qu'il est vide alors
        if (!isset($_GET["search"]) || empty(trim($_GET["search"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes ou vides"]);
            die; //! J'arrête l'exécution du script
        }

        // J'effectue une sélection en cherchant une correspondance sur chaque champs de la table sauf le mot de passe
        $req = $db->prepare("SELECT id, firstname, lastname birthdate, email FROM users WHERE firstname LIKE :search OR lastname LIKE :search OR birthdate LIKE :search OR email LIKE :search");
        $req->bindValue(":search", "%" . $method["search"] . "%");
        $req->execute();

        $users = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true et les utilisateurs trouvés
        echo json_encode(["success" => true, "users" => $users]);
        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}

