<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../utils/db_connect.php");

// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;


//? En fonction du paramètre "choice" de ma requête j'execute les instructions de la case correspondante
switch ($method["choice"]) {
    case "select":
        //TODO Modifier la requête suivante afin d'ajouter une colonne pour savoir si l'article en question est en favori ou non de l'utilisateur connecté (possibilité de retourner true ou false sur la colonne)

        // Je récupère tous les articles
        // Je récupère tous les articles, leur auteur ainsi que s'ils sont en favoris de l'utilisateur connecté
        $req = $db->prepare("SELECT a.*, CONCAT(firstname, ' ', lastname) AS author, IF(ufa.user_id IS NOT NULL, true, false) AS fav FROM articles a INNER JOIN users u ON a.user_id = u.id LEFT JOIN user_favorite_article ufa ON a.id = ufa.article_id AND ufa.user_id = ? ORDER BY created_at DESC");
        $req->execute([$_SESSION["user_id"]]);
        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        foreach ($articles as &$article) {
            $req = $db->prepare("SELECT name FROM categories c INNER JOIN article_category ac ON ac.category_id = c.id WHERE ac.article_id = ?");
            $req->execute([$article["id"]]);

            $article["categories"] = $req->fetchAll(PDO::FETCH_COLUMN);
        }

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case "select_onlyfav":
        $req = $db->prepare("SELECT a.*, CONCAT(firstname, ' ', lastname) AS author, 1 AS fav FROM articles a INNER JOIN users u ON a.user_id = u.id LEFT JOIN user_favorite_article ufa ON a.id = ufa.article_id WHERE ufa.user_id = ? ORDER BY created_at DESC");
        $req->execute([$_SESSION["user_id"]]);

        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'itère sur mes articles afin de récupérer mes catégories.
        //! J'utilise le passage par référence afin de garder les modifications effectuer sur chaque article
        foreach ($articles as &$article) {
            $req = $db->prepare("SELECT c.* FROM categories c INNER JOIN article_category ac ON ac.category_id = c.id WHERE ac.article_id = ?");
            $req->execute([$article["id"]]);

            $article["categories"] = $req->fetchAll(PDO::FETCH_ASSOC);
        }

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case "favorite":

        //? Si je n'ai pas de paramètre "article_id" et "favorite" OU qu'il est vide alors
        if (!isset($method["article_id"], $method["favorite"]) || empty(trim($method["article_id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si l'article n'est pas un favori alors j'écris la requête d'insertion
        if ($method["favorite"] == 0) {
            $req = $db->prepare("INSERT INTO user_favorite_article(article_id, user_id) VALUES (:article_id, :user_id)");
        } else { //? Sinon je crée la requête de suppression
            $req = $db->prepare("DELETE FROM user_favorite_article 
            WHERE article_id = :article_id AND user_id = :user_id");
        }

        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":article_id", $method["article_id"]);
        $req->bindValue(":user_id", $_SESSION["user_id"]);
        $req->execute();

        // J'envoie une réponse avec un success true
        echo json_encode(["success" => true]);
        break;




        //TODO Faire les vérifications nécessaires

        //TODO Si je dois ajouter l'article en favori faire la requete nécessaire
        //TODO Sinon supprimer l'article des favori en faisant la requête nécessaire

        //TODO Si tout s'est bien passé renvoyer un succes true
        break;

    case "select_id":
        //? Si je n'ai pas le paramètre "article_id" OU s'il est vide dans ma requête GET stockée dans ma variable $method alors
        if (!isset($method["article_id"]) || empty(trim($method["article_id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        };

        // Je récupère l'article ciblé par l'id en paramètre
        $req = $db->prepare("SELECT * FROM articles WHERE id = ?");
        $req->execute([$method["article_id"]]);

        // J'affecte à ma variable $article le résultat unique (ou pas de résultat) de ma requete SQL
        $article = $req->fetch(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les données de l'article
        echo json_encode(["success" => true, "article" => $article]);

        break;



    case "insert":
        //? Si la requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas les paramètres "name" et "desc" OU qu'ils sont vides alors
        if (!isset($method["name"], $method["desc"]) || empty(trim($method["name"])) || empty(trim($method["desc"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        $img = false; // Je défini img à false par défaut
        if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload

        // J'écris une requete préparée d'insertion de mes données dans la table article
        $req = $db->prepare("INSERT INTO articles(name, description, user_id, image) VALUES (:name, :desc, :user_id, :img)");
        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":name", $method["name"]);
        $req->bindValue(":desc", $method["desc"]);
        $req->bindValue(":user_id", $_SESSION["user_id"]);

        if ($img) $req->bindValue(":img", $img); // Je bind le chemin si la réponse de upload() n'est pas false
        else $req->bindValue(":img", null); // Sinon je mets l'image à null

        $req->execute();

        // J'envoie une réponse avec un success true, l'id de l'article que je viens d'insérer ainsi que l'image
        echo json_encode(["success" => true, "id" => $db->lastInsertId(), "image" => $img]);

        break;

    case "update":
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas les paramètres "name", "desc" et "id" OU qu'ils sont vides alors
        if (!isset($method["name"], $method["desc"], $method["id"]) || empty(trim($method["name"])) || empty(trim($method["desc"])) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        $img = false; // Je défini img à false par défaut
        if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload

        $img_req = ''; // Par défaut rien n'est ajouté dans la requête SQL
        if ($img) $img_req = ", image = :img"; // S'il y a une image d'upload lors de la mise à jour je rajoute le bout de requête SQL correspondant

        // J'écris une requete préparée de mise à jour de l'article si j'en suis l'auteur
        $req = $db->prepare("UPDATE articles SET name = :name, description = :desc $img_req WHERE id = :id AND user_id = :user_id");
        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":name", $method["name"]);
        $req->bindValue(":desc", $method["desc"]);
        $req->bindValue(":id", $method["id"]);
        $req->bindValue(":user_id", $_SESSION["user_id"]);
        if ($img) $req->bindValue(":img", $img);
        $req->execute();

        //? Si j'ai 1 résultat avec c'est un succès
        if ($req->rowCount()) echo json_encode(["success" => true, "image" => $img]);
        //? Sinon c'est que je ne suis pas propriétaire
        else echo json_encode(["success" => false, "error" => "Vous n'êtes pas propriétaire"]);

        break;

    case "delete":
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas de paramètre "id" OU qu'il est vide alors
        if (!isset($method["id"]) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée de suppression de l'article si j'en suis l'auteur
        $req = $db->prepare("DELETE FROM articles WHERE id = ? AND user_id = ?");
        $req->execute([$method["id"], $_SESSION["user_id"]]);

        //? Si j'ai 1 résultat avec c'est un succès
        if ($req->rowCount()) echo json_encode(["success" => true]);
        //? Sinon c'est que je ne suis pas propriétaire
        else echo json_encode(["success" => false, "error" => "Vous n'êtes pas propriétaire"]);

        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
