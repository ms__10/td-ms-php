<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../utils/db_connect.php");

// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();

//? Si ma méthode de requête est différente de POST
if ($_SERVER["REQUEST_METHOD"] != "POST") {
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
    die; //! J'arrête l'exécution du script
}

//? Si je n'ai les paramètres "firstname", "lastname" et "birthdate" OU qu'ils sont vides alors
if (!isset($_POST["firstname"], $_POST["lastname"], $_POST["birthdate"]) || empty(trim($_POST["firstname"])) || empty(trim($_POST["lastname"])) || empty(trim($_POST["birthdate"]))) {
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Données manquantes ou vides"]);
    die; //! J'arrête l'exécution du script
}

// Je déclare une variable $pwdsql afin de rajouter un bout de requête de mise à jour
$pwdsql = '';
//? Si j'ai le paramètre "password" et qu'il est non vide alors
if (isset($_POST["password"]) && !empty(trim($_POST["password"]))) {
    // J'affecte à ma variable $pwdsql le bout de requête de mise à jour pour le mot de passe
    $pwdsql = ", pwd = :pwd";

    //? Si le mot de passe ne correspond pas au format de l'ER alors
    $regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
    if (!preg_match($regex, $_POST["password"])) {
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Mot de passe au mauvais format"]);
        die; //! J'arrête l'exécution du script
    }

    // Je hash le mot de passe avec la méthode par défaut
    $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
}

// J'écris une requete préparée de mise à jour de l'utilisateur
$req = $db->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname, birthdate = :birthdate $pwdsql WHERE id = :id");
// J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
$req->bindValue(":firstname", $_POST["firstname"]);
$req->bindValue(":lastname", $_POST["lastname"]);
$req->bindValue(":birthdate", $_POST["birthdate"]);
$req->bindValue(":id", $_SESSION["user_id"]);
//? Si $pwdsql est non vide alors j'affecte à la clé du mot de passe le hash de celui passé en paramètre de la requête
if ($pwdsql != '') $req->bindValue(":pwd", $hash);
$req->execute(); // J'execute ma requete

// J'envoie une réponse avec un success true
echo json_encode(["success" => true]);