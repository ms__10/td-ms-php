// Je prérempli les champs du formulaire
$("#firstname").val(user.firstname);
$("#lastname").val(user.lastname);
$("#birthdate").val(user.birthdate);
$("#email").val(user.email);

$("form").submit(event => {
    event.preventDefault(); // J'empêche le comportement par défaut de l'événement. Ici la soumission du formulaire recharge la page

    $(".box input[type='submit']").removeClass(); // J'enlève la ou les classe de l'input submit afin d'empêcher le bug de couleur avec un cumul de classe

    // Je récupère les valeurs des champs du formuaire
    const firstname = $("#firstname").val();
    const lastname = $("#lastname").val();
    const birthdate = $("#birthdate").val();
    const password = $("#password").val();

    // J'effectue l'appel AJAX pour faire la mise à jour
    $.ajax({
        url: "../php/profile.php",
        type: "POST",
        dataType: "json",
        data: {
            firstname,
            lastname,
            birthdate,
            password
        },
        success: (res) => {
            //? Si c'est un success
            if (res.success) {
                // Je mets à jour le nom et prénom dans le menu
                $("aside p").text(firstname + " " + lastname); // Je place le nom d'utilisateur dans mon menu

                // Je mets à jour les informations dans la variable user
                user.firstname = firstname;
                user.lastname = lastname;
                user.birthdate = birthdate;

                // Je mets à jour ces informations dans le localStorage
                localStorage.setItem("user", JSON.stringify(user));

                // J'applique la couleur verte à l'input submit afin de signaler que tout s'est bien passé
                $("input[type='submit']").addClass("grass");
            } else {
                // J'applique la couleur rouge à l'input submit afin de signaler qu'il y a eu une erreur
                $("input[type='submit']").addClass("salmon");
                alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
            }
        }
    });
});