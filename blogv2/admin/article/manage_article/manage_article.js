//? Si mon utilisateur n'est pas admin je redirige vers l'accueil
if (user.admin == 0) window.location.replace("../../../article/article.html");

const urlParams = new URLSearchParams(window.location.search); // Je récupère les paramètres de mon url
const id = urlParams.get("id"); // Je récupère l'id de l'article à modifier dans l'url

/**
 * @desc Fait appel au php pour insérer un article
 * @param FormData fd - Contient les données du formulaire à envoyer
 * @return void - Ne retourne rien
 */
function insertArticle(fd) {
    fd.append("choice", "insert");

    $.ajax({
        url: "../../../php/admin/article.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue        
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            if (res.success) window.location.replace("../article.html"); //? Si success alors je redirige vers la liste des articles
            else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
}
/**
 * @desc Fait appel au php pour mettre à jour un article
 * @param FormData fd - Contient les données du formulaire à envoyer
 * @return void - Ne retourne rien
 */
function updateArticle(fd) {
    fd.append("choice", "update");
    fd.append("id", id);

    $.ajax({
        url: "../../../php/admin/article.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            if (res.success) window.location.replace("../article.html"); //? Si success alors je redirige vers la liste des articles
            else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
}
//? Si j'ai un id alors je récupère les données de l'article cible
function getArticle() {
    //? Si j'ai un id alors je récupère les données de l'article cible
    $.ajax({
        url: "../../../php/admin/article.php", // URL cible
        type: "GET", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: { // Donnée(s) à envoyer s'il y en a
            choice: "select_id",
            id
        },
        success: (res) => {
            if (res.success) {
                // Je modifie mon titre du header avec le nom de mon article
                $("header h1").text("Mise à jour de l'article " + res.article.name);
                $(".box h1").text("Mise à jour");

                // J'affecte au champs de mon formulaire les valeurs de mon article
                $("#name").val(res.article.name);
                $("#desc").val(res.article.description);

                // Je parcours les id des catégories de l'article
                res.article.categories.forEach(cat => {
                    // Je coche les checkbox associées à l'article
                    $(":checkbox[value=" + cat + "]").prop("checked", true);
                });
            } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
}

$("form").submit(event => {
    event.preventDefault(); // J'empêche le comportement par défaut de l'événement. Ici la soumission du formulaire recharge la page

    const cat_ids = [];
    $("input:checkbox:checked").each(function () {
        cat_ids.push($(this).val());
    });

    const fd = new FormData();
    // Je récupère les données du formulaire
    fd.append("name", $("#name").val());
    fd.append("desc", $("#desc").val());
    fd.append("picture", $('#picture')[0].files[0]);
    fd.append("cat_ids", JSON.stringify(cat_ids));


    if (id) updateArticle(fd);
    else insertArticle(fd);
});

// Je récupère les données de toutes mes catégories
$.ajax({
    url: "../../../php/admin/category.php",
    type: "GET",
    dataType: "json",
    data: {
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            // J'itère sur les catégories que je reçois
            res.category.forEach(cat => {
                // Pour chaque catégorie je crée une div qui contiendra un input checkbox et un label
                const checkDiv = $("<div></div>");
                checkDiv.addClass("checkDiv");

                //! Les attributs de l'input sont très important
                const checkInput = $("<input>");
                checkInput.attr("type", "checkbox");
                checkInput.attr("name", "categories");
                checkInput.val(cat.id);

                const label = $("<label></label>").text(cat.name);

                checkDiv.append(checkInput, label)
                $("#cat_ctn").append(checkDiv); // J'ajoute ma div à mon form
            });
            if (id) getArticle()
            else {
                // Je modifie mon titre du header pour inserer
                $("header h1").text("Ajouter un article");
                $(".box h1").text("Ajout");
            }

        } else alert(res.error);
    }
});