if (user.admin == 0) window.location.replace("../../../article/article.html");

const urlParams = new URLSearchParams(window.location.search); // Je récupère les paramètres de mon url
const id = urlParams.get("id"); // Je récupère l'id de la catégorie à modifier dans l'url

/**
 * @desc Fait appel au php pour insérer une catégorie
 * @param string name - Contient le nom de la catégorie
 * @return void - Ne retourne rien
 */
function insertCategory(name) {
    $.ajax({
        url: "../../../php/admin/category.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue        
        data: {
            choice: "insert",
            name
        },
        success: (res) => {
            if (res.success) window.location.replace("../category.html"); //? Si success alors je redirige vers la liste des catégories
            else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
}

/**
 * @desc Fait appel au php pour mettre à jour une catégorie
 * @param string name - Contient le nom de la catégorie
 * @return void - Ne retourne rien
 */
function updateCategory(name) {
    $.ajax({
        url: "../../../php/admin/category.php",// URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: {
            choice: "update",
            name,
            id
        },
        success: (res) => {
            if (res.success) window.location.replace("../category.html"); //? Si success alors je redirige vers la liste des catégories
            else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
}

//? Si j'ai un id alors je récupère les données de la catégorie cible

if (id) {
    $.ajax({
        url: "../../../php/admin/category.php",// URL cible
        type: "GET", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: { // Donnée(s) à envoyer s'il y en a
            choice: "select_id",
            id
        },
        success: (res) => {
            if (res.success) {
                // Je modifie mon titre du header avec le nom de ma catégorie
                $("header h1").text("Mise à jour de la catégorie " + res.category.name);
                $(".box h1").text("Mise à jour");
    
                // J'affecte au champs de mon formulaire les valeurs de ma catégorie
                $("#name").val(res.category.name);
            } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
        }
    });
} else {
    // Je modifie mon titre du header pour inserer
    $("header h1").text("Ajouter une catégorie");
    $(".box h1").text("Ajout");
}

$("form").submit(event => {
    event.preventDefault(); // J'empêche le comportement par défaut de l'événement. Ici la soumission du formulaire recharge la page

    // Je récupère les données du formulaire
    const name = $("#name").val();

    if (id) updateCategory(name);
    else insertCategory(name);
});