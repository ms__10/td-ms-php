if (user.admin == 0) window.location.replace("../../article/article.html");

/**
 * @desc Fait appel au php pour supprimer une categorie
 * @param string id - Contient l'id de la categorie
 * @return void - Ne retourne rien
 */
function deleteCategory(id) {
    $.ajax({
        url: "../../php/admin/category.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: { // Donnée(s) à envoyer s'il y en a
            choice: "delete",
            id
        },
        success: () => {
            $("#tr_" + id).remove(); // Je retire la ligne du tableau associé à la catégorie
        }
    });
}

$.ajax({
    url: "../../php/admin/category.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            res.category.forEach(cat => {
                const tr = $("<tr></tr>"); // Je crée une nouvelle ligne
                tr.attr("id", "tr_" + cat.id) // J'ajoute un id à mon tr

                const name = $("<td></td>").text(cat.name); // Je crée une case pour le nom

                const updatectn = $("<td></td>");
                const updatebtn = $("<button></button>");
                updatebtn.addClass("btn ocean action_btn"); // J'ajoute des classes sur le bouton pour le style
                updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute un texte au lien
                updatectn.append(updatebtn); // J'ajoute le boutton au td

                updatebtn.click(() => {
                    window.location.replace("manage_category/manage_category.html?id=" + cat.id); // Je redirige vers la page du formulaire avec paramètre id de ma catégorie sur laquelle j'itère en paramètre
                });

                const delctn = $("<td></td>");
                const delbtn = $("<button></button>"); // Je crée un élément bouton
                delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de poubelle
                delbtn.addClass("btn salmon action_btn"); // J'ajoute des classes sur le bouton pour le style
                delctn.append(delbtn);

                // J'ajoute un écouteur d'événement clic sur le bouton
                delbtn.click(() => {
                    if (confirm("Voulez-vous vraiment supprimer la catégorie ?")) {
                        // J'appelle la fonction wantToDelete pour demander la suppression de la catégorie
                        deleteCategory(cat.id);
                    }
                });

                tr.append(name, updatectn, delctn); // J'ajoute toutes mes cases dans ma ligne
                $("tbody").append(tr); // J'ajoute ma ligne à ma table
            });

            $("td").addClass("text-left"); // J'ajoute une classe à tous les td
        } else alert(res.error);
    }
});

// Au clic de la div "Ajouter une catégorie"
$("header div.btn").click(() => {
    // Je redirige vers la page du formulaire
    window.location.replace("manage_category/manage_category.html");
});