const urlParams = new URLSearchParams(window.location.search);
const onlyfav = urlParams.get("onlyfav"); // Je récupère le paramètre onlyfav de mon url

// Variable qui nous servira à savoir si nous voulons insérer ou mettre à jour un article
let article_id = null;

// Par défaut la récupération des articles se fait via le select basique
let select = "select";

//? Si j'ai le paramètre onlyfav dans mon url alors
if (onlyfav) {
    // Je change le choice de la récupération des articles
    select = "select_onlyfav";

    // Je modifie le titre de l'onglet et le titre de la page pour les favoris
    $("title").text("Liste des favoris");
    $("header h1").text("Liste des favories");
}

/**
 * @desc Ajoute le texte "Aucun article" si la page est vide
 * @return void - Ne retourne rien
 */
function checkIfEmpty() {
    if ($("#art_ctn").is(":empty")) {
        const none_article = $("<h2></h2>").text("Aucun article");
        $("#art_ctn").html(none_article);
    }
}

/**
 * @desc Ajoute un article sur la page HTML
 * @param object article - Contient les informations d'un article
 * @return void - Ne retourne rien
 */
function addArticle(article) {
    const ctn = $("<article></article>"); // Je crée un élement article
    ctn.addClass("art_box"); // J'ajoute à mon élément article la classe 'art_box' 
    ctn.attr("id", "article_" + article.id); // J'ajoute à mon élément article l'id 'article_' + son id 

    const title = $("<h2></h2>").text(article.name); // Je crée un élément h2 et je lui ajoute le texte correspondant au nom de mon article

    const desc_ctn = $("<div></div>"); // Je crée le conteneur de l'article et l'image
    const desc = $("<p></p>").text(article.description); // Je crée un élément p et je lui ajoute le texte correspondant à la description de mon article
    desc.addClass("desc"); // J'ajoute la classe desc au paragraphe de la description

    let img; // Je déclare la variable img sans valeur;
    if (article.image) img = $("<img>").attr("src", "../assets/" + article.image); // Je crée une image et j'affecte la source

    const author = $("<p></p>").text(article.author); // Je crée un élément p et je lui ajoute le texte correspondant à l'auteur de mon article

    const created_at = $("<small></small>"); // Je crée un élément small

    let date; // Je crée une variable date qui va contenir la date de création de l'article

    //? Si l'article à une date de création je l'affecte à la variable date
    if (article.created_at) date = new Date(article.created_at);
    //? Sinon je crée une date
    else date = new Date();

    let day = date.getDate(); // J'attribue à une variable la valeur du jour de la date que j'ai créée.
    if (day < 10) day = '0' + day; // Si le jour est inférieur à 10 je rajoute un 0 devant

    let month = date.getMonth() + 1; // J'attribue à une variable la valeur du mois de la date que j'ai créée. J'ajoute +1 car le mois commence à 0
    if (month < 10) month = '0' + month; // Si le mois est inférieur à 10 je rajoute un 0 devant

    let hours = date.getHours(); // J'attribue à une variable la valeur des heures de la date que j'ai créée.
    if (hours < 10) hours = '0' + hours; // Si l'heure est inférieur à 10 je rajoute un 0 devant

    let min = date.getMinutes(); // J'attribue à une variable la valeur ddes minutes de la date que j'ai créée.
    if (min < 10) min = '0' + min; // Si les minutes sont inférieurs à 10 je rajoute un 0 devant

    // J'attribue à l'element small le texte correspondant à la date de création de mon article au format jj/mm/yyyy hh:mm
    created_at.text(day + "/" + month + "/" + date.getFullYear() + " " + hours + "h" + min);

    const artcat_ctn = $("<div></div>");
    artcat_ctn.addClass("artcat_ctn");

    article.categories.forEach(cat => {
        const cat_btn = $("<button></button>");
        cat_btn.prop("disabled", true);
        cat_btn.addClass("btn regular cat_btn");
        cat_btn.text(cat.name);

        artcat_ctn.append(cat_btn)
    });

    const bottom_ctn = $("<div></div>"); // Je crée un conteneur où placer l'auteur, la date, les boutons et les catégories
    bottom_ctn.append(artcat_ctn, author, created_at);

    //? Si mon id utilisateur est le même que celui de l'auteur de l'article OU que je n'ai pas de date de création dans l'article alors
    if (user.id == article.user_id || !article.created_at) {
        const delete_btn = $("<button></button>"); // Je crée un bouton supprimer
        delete_btn.html("<i class='fa fa-trash'></i>"); // J'ajoute une icone de poubelle à mon bouton
        delete_btn.addClass("btn salmon action_btn"); // J'ajoute du style à mon bouton

        // J'ajoute un écouteur d'événement clic sur le bouton supprimer
        delete_btn.click(() => {
            //? Si confirmation de l'utilisateur alors j'appelle la fonction deleteArticle
            if (confirm("Êtes-vous sur de vouloir supprimer cet article ?")) deleteArticle(article.id);
        });

        const update_btn = $("<button></button>"); // Je crée un bouton modifier
        update_btn.html("<i class='fa fa-pencil'></i>"); // J'ajoute une icone de stylo à mon bouton
        update_btn.addClass("btn ocean action_btn"); // J'ajoute du style à mon bouton

        // J'ajoute un écouteur d'événement clic sur le bouton modifier
        update_btn.click(() => {
            // Je modifie le titre de la box du fomulaire
            $(".box h1").text("Modifier un article");

            // J'affecte à la variable définit en haut du fichier la valeur de l'id de l'article sur lequel on a cliqué
            article_id = article.id;

            // Je préremplie mes champs de formulaire
            $("#name").val(article.name);
            $("#desc").val(article.description);

            // Je parcours les id des catégories de l'article
            article.categories.forEach(({
                id
            }) => {
                // Je coche les checkbox associées à l'article
                $(":checkbox[value=" + id + "]").prop("checked", true);
            });

            $(".box").addClass("open"); // J'affiche mon formulaire
            $("#overlay").addClass("open"); // J'affiche mon overlay
        });

        // J'ajoute mes boutons au conteneur des boutons
        bottom_ctn.append(update_btn, delete_btn);
    }

    const favorite_btn = $("<button></button>"); // Je crée un élément bouton
    favorite_btn.addClass("btn action_btn favorite"); // Je lui affecte des classes
    favorite_btn.attr("data-favorite", article.fav); // Je lui ajoute un attribut de donnée pour le favori

    //? Si c'est un favori l'étoile est pleine sinon elle est vide
    const icon = article.fav == 1 ? "star" : "star-o";
    const icon_fav = $("<i aria-hidden='true'></i>");
    icon_fav.addClass("fa fa-" + icon);

    // J'ajoute l'icone au bouton
    favorite_btn.append(icon_fav);
    // J'ajoute le bouton à mes autres boutons
    bottom_ctn.append(favorite_btn);

    // J'ajoute un évenement click sur le bouton de mes favoris
    favorite_btn.click(() => {
        // J'appelle la fonction manageFavorite afin de mettre à jour mon favori
        manageFavorite(article.id);
    });

    // J'ajoute les éléments description et image dans un conteneur
    desc_ctn.append(desc, img);

    ctn.append(title, desc_ctn, bottom_ctn); // J'ajoute les éléments titre, description avec image, auteur, date de création et les boutons dans mon article

    //? Si j'ai une date de création de l'article alors c'est un article existant je l'ajoute dans la liste
    if (article.created_at) $("#art_ctn").append(ctn);
    //? Sinon c'est un nouvel article je l'ajoute au début de la liste
    else $("#art_ctn").prepend(ctn);
}

/**
 * @desc Fait appel au php pour supprimer un article
 * @param string id - Contient l'id de l'article
 * @return void - Ne retourne rien
 */
function deleteArticle(id) {
    $.ajax({
        url: "../php/article.php",
        type: "POST",
        dataType: "json",
        data: {
            choice: "delete",
            id
        },
        success: (res) => {
            //? Si la réponse est un succès alors je supprime l'article HTML correspondant
            if (res.success) $("#article_" + id).remove();
            else alert(res.error);
        }
    });
}

/**
 * @desc Fait appel au php pour inserer un article
 * @param string name - Contient le nom de l'article
 * @param string desc - Contient la description de l'article
 * @return void - Ne retourne rien
 */
function insertArticle(name, desc, picture, cat_ids) {
    // Je crée une nouvelle instance de FormData afin d'ajouter chaque de mes clés
    const fd = new FormData();
    fd.append("choice", "insert");
    fd.append("name", name);
    fd.append("desc", desc);
    fd.append("cat_ids", JSON.stringify(cat_ids));
    fd.append("picture", picture);

    $.ajax({
        url: "../php/article.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        data: fd,
        success: (res) => {
            if (res.success) {
                // J'appelle la fonction addArticle afin de créer un article par rapport aux données du formulaire
                addArticle({
                    id: res.id,
                    name,
                    description: desc,
                    author: user.firstname + " " + user.lastname,
                    image: res.image,
                    categories: res.categories
                });

                $(".box").removeClass("open"); // Je cache mon formulaire
                $("#overlay").removeClass("open"); // Je cache mon overlay
            } else alert(res.error);
        }
    });
}

/**
 * @desc Fait appel au php pour mettre à jour un article
 * @param string name - Contient le nom de l'article
 * @param string desc - Contient la description de l'article
 * @return void - Ne retourne rien
 */
function updateArticle(name, desc, picture, cat_ids) {
    const fd = new FormData();
    fd.append("choice", "update");
    fd.append("name", name);
    fd.append("desc", desc);
    fd.append("picture", picture);
    fd.append("cat_ids", JSON.stringify(cat_ids));
    fd.append("id", article_id);

    $.ajax({
        url: "../php/article.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        data: fd,
        success: (res) => {
            //? Si la réponse renvoie un succès alors
            if (res.success) {
                //* Je modifie chacune des nouvelles données
                $("#article_" + article_id + " h2").text(name);
                $("#article_" + article_id + " p.desc").text(desc);
                $("#article_" + article_id + " img").attr("src", "../assets/" + res.image);

                $("#article_" + article_id + " .artcat_ctn").html("");
                res.categories.forEach(cat => {

                    const cat_btn = $("<button></button>");
                    cat_btn.prop("disabled", true);
                    cat_btn.addClass("btn regular cat_btn");
                    cat_btn.text(cat.name);

                    $("#article_" + article_id + " .artcat_ctn").append(cat_btn)
                });
            }

            // Je défini mon article_id à null, ma modification est terminée
            article_id = null;

            $(".box").removeClass("open"); // Je cache mon formulaire
            $("#overlay").removeClass("open"); // Je cache mon overlay
        }
    });
}

/**
 * @desc Ajoute ou supprime un favori sur l'article cible'
 * @param int article_id - Contient l'identifiant d'un article
 * @return void - Ne retourne rien
 */
function manageFavorite(article_id) {
    // Je récupère la donnée du favori de l'article ciblé grâce à l'attribut de donnée
    let favorite = $("#article_" + article_id + " .favorite").attr("data-favorite");

    // J'effectue l'ajout ou la suppression
    $.ajax({
        url: "../php/article.php",
        type: "POST",
        dataType: "json",
        data: {
            choice: "favorite",
            article_id,
            favorite
        },
        success: (res) => {
            //? Si c'est un success
            if (res.success) {
                let icon = '';
                //? Si ce n'était pas un favori alors
                if (favorite == 0) {
                    favorite = 1;
                    icon = 'star';
                } else { //? Sinon
                    favorite = 0;
                    icon = 'star-o'
                }

                // Je modifie l'attribut de donnée "favorite" avec la nouvelle valeur
                $("#article_" + article_id + " .favorite").attr("data-favorite", favorite);

                // Je détermine la nouvelle icone du favori
                const icon_fav = $("<i aria-hidden='true'></i>");
                icon_fav.addClass("fa fa-" + icon);

                // Je modifie la nouvelle icone
                $("#article_" + article_id + " .favorite").html(icon_fav);

                //? Si j'affiche seulement les favoris alors je supprime l'article que je viens d'enlever de mes favoris
                if (onlyfav && favorite == 0) $("#article_" + article_id).remove();

                checkIfEmpty(); // Je check s'il reste du contenu sur la page
            } else alert(res.error);
        }
    });
}

// J'effectue un appel AJAX pour récupérer tous les articles
$.ajax({
    url: "../php/article.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: select
    },
    success: (res) => {
        //? Si la réponse est un succès alors
        if (res.success) {
            // J'itère sur les articles que je récupère
            res.articles.forEach(art => {
                // J'appelle la fonction addArticle afin de créer un article par élement de mon tableau articles
                addArticle(art);
            });

            checkIfEmpty(); // Je check s'il reste du contenu sur la page
        } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur
    }
});

$("form").submit(event => {
    event.preventDefault(); // Je préviens le comportement par défaut du formulaire pour empecher la page de se recharger

    const cat_ids = [];
    $("input:checkbox:checked").each(function () {
        cat_ids.push($(this).val());
    });

    // Je récupère dans des variables les valeurs des champs de mon formulaire à la soumission
    const name = $("#name").val();
    const desc = $("#desc").val();
    const picture = $("#picture")[0].files[0];

    //? Si j'ai une valeur dans la variable article_id alors je mets à jour. Sinon j'insère.
    if (article_id) updateArticle(name, desc, picture, cat_ids);
    else insertArticle(name, desc, picture, cat_ids);
});

// Au clic de la div "Ajouter un article"
$("header div.btn").click(() => {
    $(".box h1").text("Ajouter un article"); // Je mets à jour le titre du formulaire

    document.querySelector("form").reset(); // Je vide le formulaire

    $(".box").addClass("open"); // J'affiche mon formulaire
    $("#overlay").addClass("open"); // J'affiche mon overlay
});

// Au clic de l'overlay
$("#overlay").click(() => {
    $(".box").removeClass("open"); // Je cache mon formulaire
    $("#overlay").removeClass("open"); // Je cache mon overlay
});

// Je récupère les données de toutes mes catégories
$.ajax({
    url: "../php/category.php",
    type: "GET",
    dataType: "json",
    data: {
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            // J'itère sur les catégories que je reçois
            res.categories.forEach(cat => {
                // Pour chaque catégorie je crée une div qui contiendra un input checkbox et un label
                const checkDiv = $("<div></div>");
                checkDiv.addClass("checkDiv");

                //! Les attributs de l'input sont très important
                const checkInput = $("<input>");
                checkInput.attr("type", "checkbox");
                checkInput.attr("name", "categories");
                checkInput.val(cat.id);

                const label = $("<label></label>").text(cat.name);

                checkDiv.append(checkInput, label)
                $("#cat_ctn").append(checkDiv); // J'ajoute ma div à mon form
            });
        } else alert(res.error);
    }
});