<?php

//? Je crée une boucle allant de 200 à 250 pas de 1
for ($i = 200; $i <= 250; $i++) {
    //? Pour chaque valeur si le reste de la division par 4 est 0 alors
    if ($i % 4 == 0) {
        echo $i . " ";
    }
}

echo "<br>";

//? Je crée une boucle sur un interval allant de 200 à 250
foreach (range(200, 250) as $n) {
    //? Pour chaque valeur si le reste de la division par 4 est 0 alors
    if ($n % 4 == 0) {
        echo $n . " ";
    }
}

echo "<br>";

// J'explose l'interval allant de 200 à 250 pas de 4 avec comme séparateur un espace
echo implode(" ", range(200, 250, 4));
