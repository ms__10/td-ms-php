<?php

function palindrome($word)
{
    if ($word == strrev($word)) {
        echo "$word est un palindrome";
    } else {
        echo "$word n'est pas un palindrome, son inverse est " . strrev($word);
    }
    echo "<br>";
}

palindrome("bob");
palindrome("radar");
palindrome("chaussette");