<?php

/**
 * @desc Affiche si un nombre est un multiple de 3 ou de 7
 * @param int n - Contient un entier
 * @return void - Ne retourne rien
 */
function multiple($n)
{
    //? Si le reste de la division par 3 et par 7 est 0 alors
    if ($n % 3 == 0 && $n % 7 == 0) {
        echo "$n est un multiple de 3 et de 7";
    } else if ($n % 3 == 0) { //? Sinon si le reste de la division par 3 est 0 alors
        echo "$n est un multiple de 3";
    } else if ($n % 7 == 0) { //? Sinon si le reste de la division par 7 est 0 alors
        echo "$n est un multiple de 7";
    } else { //? Sinon
        echo "$n est pas un multiple de 3 ou de 7";
    }
    echo "<br>";
}

multiple(3);
multiple(17);
multiple(42);
multiple(32);
