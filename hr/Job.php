<?php
//? Je récupère mon fichier de connexion à la base de données
require_once("db_connect.php");

print_r($_POST);

//? Si id et title existe dans la superglobale POST et s'ils sont non vides alors
if (
    isset($_POST["id"], $_POST["title"]) &&
    !empty(trim($_POST["id"])) &&
    !empty(trim($_POST["title"]))
) {
    //? Si min est vide alors
    if (empty(trim($_POST["min"]))) $min = 'NULL';
    else $min = $_POST["min"];

    //? Si max est vide alors
    if (empty(trim($_POST["max"]))) $max = 'NULL';
    else $max = $_POST["max"];

    //TODO Corriger quand min et max sont NULL

    //* J'écris la requete préparée d'insertion de jobs
    $req = $db->prepare("INSERT INTO jobs (job_id, job_title, min_salary, max_salary) VALUES (:id, :title, :min_salary, :max_salary)");

    //? J'accroche mes valeurs à la clé associées de la requête au dessus
    $req->bindValue(":id", $_POST["id"]);
    $req->bindValue(":title", $_POST["title"]);
    $req->bindValue(":min_salary", $min);
    $req->bindValue(":max_salary", $max);

    //? J'execute la requete
    $req->execute();
}

//* J'écris et execute la requete afin de récupérer toutes les données de tous les jobs
$req = $db->query("SELECT * FROM jobs");

//* Je récupère chacun des résultats de ma requête sous forme d'un tableau. C'est un tableau double dimension. 1ere dimension indexée qui correspond à chacun des employés. 2nd dimension associative.
$jobs = $req->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Liste des jobs</title>
</head>

<body>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Intitulé</th>
            <th>Salaire Min</th>
            <th>Salaire Max</th>
        </tr>

        <?php
        //? J'itère sur chacun des jobs
        foreach ($jobs as $job) {
            echo "<tr>";
            echo "<td>{$job['job_id']}</td>";
            echo "<td>{$job['job_title']}</td>";
            echo "<td>{$job['min_salary']}</td>";
            echo "<td>{$job['max_salary']}</td>";
            echo "</tr>";
        }
        ?>
    </table>

    <form method="POST">
        <div>
            <label for="id">ID</label>
            <input type="text" name="id" id="id" required>

            <label for="title">Intitulé</label>
            <input type="text" name="title" id="title" required>
        </div>

        <div>
            <label for="min">Salaire minimum</label>
            <input type="text" name="min" id="min">

            <label for="max">Salaire maximum</label>
            <input type="text" name="max" id="max">
        </div>
        <input type="submit">
    </form>
</body>

</html>