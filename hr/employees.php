<?php
require_once("db_connect.php");

//TODO Afficher le nom et le prénom complet dans une seule case.
//TODO A la place du job_id afficher l'intitulé du job
//TODO A la place du department_id afficher le nom du département

$req = $db->query("SELECT CONCAT(first_name, ' ', last_name) AS fullname, job_title,salary, department_name FROM employees e INNER JOIN jobs j ON j.job_id = e.job_id INNER JOIN departments d ON d.department_id = e.department_id");
$employees = $req->fetchAll(PDO::FETCH_ASSOC);

// print_r($employees[0]);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Liste des employés</title>
</head>

<body>
    <table border="1">
        <tr>
            <th>Nom complet</th>
            <th>Job</th>
            <th>Departement</th>
            <th>salary</th>
        </tr>

        <?php
        foreach ($employees as $emp) {
            echo "<tr>";
            echo "<td><a href ='employee.php'>{$emp['fullname']}</a></td>";
            echo "<td>{$emp['job_title']}</td>";
            echo "<td>{$emp['department_name']}</td>";
            echo "<td>{$emp['salary']}</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>

