<?php

$schoolCalendar = [
    "Rentrée" => [1, 9, 2020],
    "Vacances Toussaint" => [
        "début" => [17, 10, 2020],
        "fin" => [2, 11, 2020]
    ],
    "Vacances Noël" => [
        "début" => [19, 12, 2020],
        "fin" => [4, 1, 2021]
    ],
    "Vacances d'hiver" => [
        "début" => [6, 2, 2021],
        "fin" => [22, 2, 2021]
    ],
    "Vacances de printemps" => [
        "début" => [10, 4, 2021],
        "fin" => [26, 4, 2021]
    ],
    "Fin des cours" => [2, 6, 2021]
];

foreach ($schoolCalendar as $key => $val) {
    echo "- $key: ";
    if (count($val) == 2) {
        echo " du " . implode("/", $val["début"]) . " au " . implode("/", $val["fin"]);
    } else {
        echo implode("/", $val);
    }
    echo "<br>";
}
