<?php


$players = [
    ['name' => 'Martin', 'score' => 150],
    ['name' => 'Thomas', 'score' => 120],
    ['name' => 'Marie', 'score' => 98],
    ['name' => 'Nicolas', 'score' => 153],
    ['name' => 'Louise', 'score' => 118]
];
function bestPlayer($players) {
    $bestScore = 0;
    $bestPlayer = null;

    foreach ($players as $player) {
        if ($player['score'] > $bestScore) {
            $bestScore = $player['score'];
            $bestPlayer = $player;
        }
    }

    return $bestPlayer;
}


$bestPlayer = bestPlayer($players);

if ($bestPlayer !== null) {
    echo "Le meilleur joueur est " . $bestPlayer['name'] . " avec un score de " . $bestPlayer['score'];
} 