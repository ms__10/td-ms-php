<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

//? Permet de démarrer la session sur ce fichier et donc d'utiliser la super globale $_SESSION
session_start();

//? J'intègre le contenu du fichier de connexion à ma bdd dans mon fichier actuel
require_once("db_connect.php");

//? Si le paramètre "connected" présent dans ma sessionest false ou n'existe pas alors je ne suis pas connecté donc
if (!$_SESSION["connected"]) {
    //* Je redirige vers login.html
    header("Location: ./login/login.html");
}

//? Si j'ai les paramètres "name" et "desc" dans ma superglobale $_POST ET si ces paramètres sont non vides alors
if (
    isset($_POST["name"], $_POST["desc"]) &&
    !empty(trim($_POST["name"])) && !empty(trim($_POST["desc"]))
) {
    //* J'insert
    $req = $db->prepare("INSERT INTO articles(name, description, user_id) VALUES (:name, :desc, :user_id)");
    $req->bindValue(":user_id", $_SESSION["user_id"]);
    $req->bindValue(":name", $_POST["name"]);
    $req->bindValue(":desc", $_POST["desc"]);
    $req->execute();
}

//* Je selectionne tous les articles et leurs auteurs
$articles = $db->query("SELECT a.*, CONCAT(firstname, ' ', lastname) AS fullname FROM articles a INNER JOIN users u ON u.id = a.user_id");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Blog</title>
</head>

<body>
    <form method="POST">
        <div>
            <label for="name">Nom de l'article: </label>
            <input type="text" name="name" id="name" required>
        </div>

        <div>
            <label for="desc">Description: </label>
            <textarea name="desc" id="desc"></textarea>
        </div>

        <input type="submit" value="Poster un article">
    </form>

    <?php
    foreach ($articles as $article) {
        echo "<div>";
        echo "<h1>{$article["name"]}</h1>";
        echo "<p>{$article["description"]}</p>";
        echo "<p><small>{$article["created_at"]}</small></p>";
        echo "<p><small>{$article["fullname"]}</small></p>";
        echo "</div>";
    }

    //TODO Rajouter un bouton supprimer sur chaque article. Au clic du bouton on supprime l'article de la bdd (donc du front aussi)

        ?>
        <div>
            
        </div>
</body>

</html>