<?php

$host = "localhost"; // host
$username = "root"; // Nom de l'utilisateur (root)
$password = ""; //Mot de passe de l'utilisateur
$dbname = "blog"; // nom de la base de données

try{
    $db = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
   // echo "connexion OK";
} catch (ErrorException $e) {
    echo $e;
}