<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

//? J'intègre le contenu du fichier de connexion à ma bdd dans mon fichier actuel
require_once("../db_connect.php");

//? Si mes paramètres nécessaires à l'inscription n'existent pas alors
if (!isset($_POST["firstname"], $_POST["lastname"], $_POST["birthdate"], $_POST["email"], $_POST["pwd"])) {
    //* J'affiche un message d'erreur
    echo "Données manquantes";
    //! J'arrête l'exécution du reste du script
    die;
}

//? Si au moins un des paramètres est vide alors
if (
    empty(trim($_POST["firstname"])) ||
    empty(trim($_POST["lastname"])) ||
    empty(trim($_POST["birthdate"])) ||
    empty(trim($_POST["email"])) ||
    empty(trim($_POST["pwd"]))
) {
    //* J'affiche un message d'erreur
    echo "Données vides";
    //! J'arrête l'exécution du reste du script
    die;
}

$regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
//? Si mon email ne correspond pas à l'ER alors
if (!preg_match($regex, $_POST["email"])) {
    //* J'affiche un message d'erreur
    echo "Email au mauvais format";
    //! J'arrête l'exécution du reste du script
    die;
}

$regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
//? Si mon mot de passe ne correspond pas à l'ER alors
if (!preg_match($regex, $_POST["pwd"])) {
    //* J'affiche un message d'erreur
    echo "Mot de passe au mauvais format";
    //! J'arrête l'exécution du reste du script
    die;
}

//* J'insert dans ma bdd mon utilisateur
$req = $db->prepare("INSERT INTO users(firstname, lastname, birthdate, email, pwd) VALUES (:firstname, :lastname, :birthdate, :email, :pwd)");
$req->bindValue(":firstname", $_POST["firstname"]);
$req->bindValue(":lastname", $_POST["lastname"]);
$req->bindValue(":birthdate", $_POST["birthdate"]);
$req->bindValue(":email", $_POST["email"]);
$req->bindValue(":pwd", $_POST["pwd"]);
$req->execute();

//* Je redirige vers la login.html
header("Location: ../login/login.html");

