<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

//? Permet de démarrer la session sur ce fichier et donc d'utiliser la super globale $_SESSION
session_start();

//? J'intègre le contenu du fichier de connexion à ma bdd dans mon fichier actuel
require_once("../db_connect.php");

//? Si je n'ai pas les paramètres "email" et "pwd" dans ma superglobale $_POST alors
if (!isset($_POST["email"], $_POST["pwd"])) {
    echo "Données manquantes";
    die;
}

//? Si les paramètres "email" et "pwd" dans ma superglobale $_POST sont vides alors
if (empty(trim($_POST["email"])) || empty(trim($_POST["pwd"]))) {
    echo "Données vides";
    die;
}

//* Je selectionne l'id de la table users si l'email et le mot de passe reçu en paramètre de ma superglobale $_POST appartiennent à un utilisateur
$req = $db->prepare("SELECT id FROM users WHERE email = ? AND pwd = ?");
$req->execute([$_POST["email"], $_POST["pwd"]]);

//* Je récupère le premier résultat s'il y en a un (ici soit 1 soit 0)
$user = $req->fetch(PDO::FETCH_ASSOC);

//? Si l'utilisateur existe alors
if ($user) {
    //* Je crée une clé "connected" de valeur true dans ma session
    $_SESSION["connected"] = true;

    //* Je crée une clé "user_id" dans ma session qui contient l'id de mon utilisateur connecté
    $_SESSION["user_id"] = $user["id"];

    //* Je redirige vers la page d'accueil index.php
    header("Location: ../index.php");
} else {
    //! Je vide la session
    $_SESSION = [];

    //* Je redirige vers la page login.html
    header("Location: login.html");
}
