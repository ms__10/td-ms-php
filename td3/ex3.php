<?php

$sentence = "Je m'amuse en php";
$words = explode(" ", $sentence);

// print_r($words);

$max = $words[0];
foreach ($words as $word) {
    if (strlen($word) > strlen($max)) {
        $max = $word;
    }
}

echo "Le mot le plus long de ma phrase est: $max";
